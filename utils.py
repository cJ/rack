#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: MIT

import re
import hashlib


name_pat = re.compile(r".*_(?P<n>\d+)$")

def create_new_name(old: str=None):
	if old is None:
		return "new_1"
	if (m := name_pat.match(old)) is not None:
		# increase number by one
		n = m.group("n")
		suff = f"_{int(n)+1}"
		return old[:len(old)-len(n)-1] + suff
	suff = "_1"
	return old + suff


def h(x: bytes):
	"""
	Default hasher
	"""
	return hashlib.sha256(x).digest()
