#!/usr/bin/env python
# SPDX-FileCopyrightText: 2019,2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: MIT

import os
import typing as t
import logging
import tempfile

import pytest

from .set import PersistentSet

from ._test_set_funcs import *


logger = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def dir():
	with tempfile.TemporaryDirectory() as tdir:
		yield tdir

@pytest.fixture()
def s(dir, foo):
	foo1 = foo + "1"
	with PersistentSet(os.path.join(dir, "set_1")) as s:
		s.update([foo, foo1])
		yield s
		s.clear()

@pytest.fixture()
def s2(dir):
	with PersistentSet(os.path.join(dir, "set2")) as s:
		yield s
		s.clear()

@pytest.fixture(scope="module")
def foo():
	return "pouet"


