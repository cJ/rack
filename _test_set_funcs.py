#!/usr/bin/env python
# SPDX-FileCopyrightText: 2019,2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: MIT

import os
import typing as t
import logging


logger = logging.getLogger(__name__)


def test_typing(s):
	assert isinstance(s, t.Set)
	assert isinstance(s, set)
	s_ = set()
	assert isinstance(s_, t.Set)
	assert isinstance(s_, set)

def test_smoke(s, foo):
	foo2 = foo + "2"
	s.add(foo2)

	assert foo2 in s

	for x in s:
		logger.info("element: %s", x)

def test_remove(s, foo):
	assert foo in s

	s.remove(foo)

	assert foo not in s

def test_update(s, foo):
	foo2 = foo + "2"
	foo3 = foo + "3"

	s.update([foo2, foo3])
	assert foo2 in s
	assert foo3 in s

def test_pop(s):
	l = len(s)
	res = s.pop()
	assert len(s) == l-1

def test_clear(s):
	l = len(s)
	assert l >= 2
	s.clear()
	assert len(s) == 0

def test_union(s, foo, dir):
	name = os.path.join(dir, "union")
	foo3 = foo + "3"

	assert foo in s
	new = s.union([foo3], name=name)
	with new as n:
		assert foo in n
		assert foo3 in n
		s.remove(foo)
		assert foo in n

def test_issuperset(s, s2, foo):
	foo1 = foo + "1"
	foo2 = foo + "2"
	s2.update([foo, foo1])
	assert s.issuperset(s2)

	s2.add(foo2)

	assert not s.issuperset(s2)


def test_issubset(s, s2, foo):
	foo1 = foo + "1"
	foo2 = foo + "2"

	s2.update([foo, foo1, foo2])

	assert s.issubset(s2)
	assert not s.issuperset(s2)

	s2.remove(foo)

	assert not s.issubset(s2)


def test_intersection(s, s2, foo, dir):
	name = os.path.join(dir, "intersection")
	foo1 = foo + "1"
	foo2 = foo + "2"
	s2.update([foo1, foo2])

	res = s.intersection(s2, name=name)
	with res as s3:
		assert len(s3) == 1
		assert foo1 in s3
		assert foo not in s3

		s3.clear()

	s2.remove(foo1)
	res = s.intersection(s2, name=name)
	with res as s3:
		assert len(s3) == 0

		s3.clear()

def test_isdisjoint(s, s2, foo, dir):
	name = os.path.join(dir, "intersection")
	foo2 = foo + "2"
	s2.add(foo2)

	res = s.isdisjoint(s2)
	assert res

	res = s.intersection(s2, name=name)
	with res as s3:
		assert len(s3) == 0

def test_difference(s, s2, foo, dir):
	name = os.path.join(dir, "intersection")
	foo1 = foo + "1"
	foo2 = foo + "2"
	s.add(foo2)
	s2.update([foo, foo1])

	res = s.difference(s2, name=name)

	with res as s3:
		assert len(s3) == 1
		assert foo2 in s3

	s.remove(foo2)

def test_dicard(s, foo):
	foo2 = foo + "2"

	try:
		s.discard(foo2)
	except KeyError:
		assert 0, "Didn't expect exception."
	s.discard(foo)
	assert foo not in s

	s.add(foo)

def test_symmetric_difference(s, s2, foo, dir):
	name = os.path.join(dir, "symmetric_difference")
	foo1 = foo + "1"
	foo2 = foo + "2"
	foo3 = foo + "2"

	s2.update([foo2, foo3])

	res = s.symmetric_difference(s2, name=name)
	with res as s3:
		for f in [foo, foo1, foo2, foo3]:
			assert f in s3

def test_symmetric_difference_update(s, s2, foo):
	foo1 = foo + "1"
	foo2 = foo + "2"
	foo3 = foo + "2"

	s2.update([foo2, foo3])

	s.symmetric_difference_update(s2)
	for f in [foo, foo1, foo2, foo3]:
		assert f in s

def return_same_hash(x):
	return b"same"

def test_hash_collision(s, foo, dir):
	name = os.path.join(dir, "collision")
	foo1 = foo + "1"
	with type(s)(name=name, hasher=return_same_hash) as s:
		s.add(foo)
		assert foo in s

		s.add(foo1)
		assert foo1 in s
		assert foo in s

		s.remove(foo)
		assert not foo in s
		assert foo1 in s
