# Illustrate space/time tradeoffs of different implementations

import tempfile
import logging
import os
import time
import random
import itertools

try:
	from exmakhina.cbor import (loads, dumps)
except:
	from cbor2 import (loads, dumps)

from .dict_lmdb import PersistentDict as dict_lmdb
from .dict_sqlite3 import PersistentDict as dict_sqlite3


logger = logging.getLogger(__name__)


def test_int():
	random.seed(0)
	serdes = (loads,dumps)
	with tempfile.TemporaryDirectory() as _dir:
		name = os.path.join(_dir, "dict")
		with (
		 dict_lmdb(name=name, serdes=serdes) as d_lmdb,
		 dict_sqlite3(name=name, serdes=serdes) as d_sqlite3,
		):
			ks = [_ for _ in range(100000) ]
			vs = [_ for _ in range(100000) ]
			if 0:
				random.shuffle(ks)
				random.shuffle(vs)

			s = 0
			t0 = time.time()
			for k, v in zip(ks, vs):
				s += len(dumps(k)) + len(dumps(v))
			t1 = time.time()
			logger.info("nop time    : %.3f", (t1-t0))
			logger.info("nop space   : %d", s)

			t0 = time.time()
			for k, v in zip(ks, vs):
				d_lmdb[k] = v
			t1 = time.time()
			logger.info("lmdb time    : %.3f", (t1-t0))
			s = os.path.getsize(name + ".lmdb")
			logger.info("lmdb space   : %d", s)

			t0 = time.time()
			for k, v in zip(ks, vs):
				d_sqlite3[k] = v
			t1 = time.time()
			logger.info("sqlite3 time : %.3f", (t1-t0))
			s = os.path.getsize(name + ".sqlite3")
			logger.info("sqlite3 space: %d", s)


def test_overhead():
	last = time.time()
	serdes = (loads,dumps)

	s_none = 0
	with tempfile.TemporaryDirectory() as _dir:
		db_name = os.path.join(_dir, "dict")
		with (
		  dict_sqlite3(db_name, serdes=serdes) as d_sqlite3,
		  dict_lmdb(db_name, serdes=serdes) as d_lmdb,
		 ):
			for i in itertools.count():
				if i == int(os.environ.get("RACK_TEST_LIMIT", 30000)):
					break
				now = time.time()
				t = int(time.time() * 1e6)

				s_none += len(dumps(t)) + len(dumps(s_none.to_bytes(8, "big")))
				s_lmdb = os.path.getsize(d_lmdb._path)
				s_sqlite3 = os.path.getsize(d_sqlite3._path)
				if now > last + 1:
					logger.info("none    %10.6f %12d %12d", s_none/i, i, s_none)
					logger.info("sqlite3 %10.6f %12d %12d", s_sqlite3/i, i, s_sqlite3)
					logger.info("lmdb    %10.6f %12d %12d", s_lmdb/i, i, s_lmdb)
					last = now

				d_lmdb[t] = s_lmdb.to_bytes(8, "big")
				d_sqlite3[t] = s_sqlite3.to_bytes(8, "big")
