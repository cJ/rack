# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-rack@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

"""Persistent data structures mimicking as Python native containers"""

__version__ = "0.1.4"
